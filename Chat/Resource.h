//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Chat.rc
//
#define IDD_CHAT_DIALOG                 102
#define IDP_SOCKETS_INIT_FAILED         103
#define IDR_MAINFRAME                   128
#define IDD_HISTORY_DIALOG              130
#define IDC_SEND_BTN                    1001
#define IDC_MSG_EDIT                    1002
#define IDC_COMBO1                      1003
#define IDC_NAME_EDIT                   1004
#define IDC_LOG_EDIT                    1006
#define IDC_EDIT1                       1009
#define IDC_HISTORY_EDIT                1009
#define IDC_BUTTON1                     1010
#define IDC_HISTORY_BTN                 1010

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1011
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
