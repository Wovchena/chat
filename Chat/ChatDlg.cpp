// ChatDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Chat.h"
#include "ChatDlg.h"
#include "afxdialogex.h"
#include <Winuser.h>
#include "HistoryDlg.h"
#include <iostream>     // for files
#include <fstream>      // for files
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CChatDlg dialog



CChatDlg::CChatDlg(CWnd* pParent /*=NULL*/)
    : CDialogEx(CChatDlg::IDD, pParent)
    , m_MsgEdit(_T(""))
    , m_addresseeComboBox(_T(""))
    , m_nameEdit(_T("StandartName"))
    , m_logEdit(_T(""))
{
    m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CChatDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_COMBO1, m_ListComboBox);
    DDX_Control(pDX, IDC_LOG_EDIT, m_ctrlLog);
    DDX_Text(pDX, IDC_MSG_EDIT, m_MsgEdit);
    DDX_CBString(pDX, IDC_COMBO1, m_addresseeComboBox);
    DDX_Text(pDX, IDC_NAME_EDIT, m_nameEdit);
    DDX_Text(pDX, IDC_LOG_EDIT, m_logEdit);
}

BEGIN_MESSAGE_MAP(CChatDlg, CDialogEx)
    ON_WM_PAINT()
    ON_WM_QUERYDRAGICON()
    ON_BN_CLICKED(IDC_SEND_BTN, &CChatDlg::OnBnClickedSendBtn)
    ON_EN_CHANGE(IDC_NAME_EDIT, &CChatDlg::OnChangeNameEdit)
    ON_EN_CHANGE(IDC_MSG_EDIT, &CChatDlg::OnChangeMsgEdit)
    ON_MESSAGE(MY_WM_UPDATE, OnMyUpdate)
    ON_BN_CLICKED(IDC_HISTORY_BTN, &CChatDlg::OnBnClickedHistoryBtn)
END_MESSAGE_MAP()


// CChatDlg message handlers

BOOL CChatDlg::OnInitDialog()
{
    CDialogEx::OnInitDialog();

    // Set the icon for this dialog.  The framework does this automatically
    //  when the application's main window is not a dialog
    SetIcon(m_hIcon, TRUE);			// Set big icon
    SetIcon(m_hIcon, FALSE);		// Set small icon
    WSADATA wsaData;                 // Structure for WinSock setup communication 
    if (WSAStartup(MAKEWORD(2, 0), &wsaData) != 0) // Load Winsock 2.0 DLL
    {
        AfxMessageBox(_T("WSAStartup() failed"));
        exit(1);
    }
    THREADSTRUCT *_param = new THREADSTRUCT;
    _param->_this = this;
    _param->_hwnd = GetSafeHwnd();
    AfxBeginThread(recvThread, _param);
    AfxBeginThread(sendThread, _param);
    return TRUE;  // return TRUE  unless you set the focus to a control
}

void CChatDlg::OnPaint()
{
    if (IsIconic())
    {
        CPaintDC dc(this); // device context for painting

        SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

        // Center icon in client rectangle
        int cxIcon = GetSystemMetrics(SM_CXICON);
        int cyIcon = GetSystemMetrics(SM_CYICON);
        CRect rect;
        GetClientRect(&rect);
        int x = (rect.Width() - cxIcon + 1) / 2;
        int y = (rect.Height() - cyIcon + 1) / 2;

        // Draw the icon
        dc.DrawIcon(x, y, m_hIcon);
    }
    else
    {
        CDialogEx::OnPaint();
    }
}

HCURSOR CChatDlg::OnQueryDragIcon()
{
    return static_cast<HCURSOR>(m_hIcon);
}

LRESULT CChatDlg::OnMyUpdate(UINT wParam, LONG lParam)
{
    UpdateData(FALSE);
    return 0;
}

UINT CChatDlg::sendThread(LPVOID param)
{
    THREADSTRUCT* ts = (THREADSTRUCT*)param;
    const unsigned short PORT = 1027;
    int sock;                        /* Socket */
    int broadcastPermission = 1;
    struct sockaddr_in addr; /* Local address */
    if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
    {
        AfxMessageBox(_T("socket!"));
    }
    if (setsockopt(sock, SOL_SOCKET, SO_BROADCAST, (const char *)&broadcastPermission, sizeof(broadcastPermission)) < 0) {
        AfxMessageBox(_T("setsockopt!"));
    }
    memset(&addr, 0, sizeof(addr));    // Zero out structure 
    addr.sin_family = AF_INET;                  //Internet address family 
    addr.sin_addr.s_addr = htonl(INADDR_BROADCAST); // Server IP address 
    addr.sin_port = htons(PORT);     /* Server port */
    char * msg = "Hello";

    for (unsigned int i = 0;; i++)
    {
        if (i != 0)
        {
            ts->_this->cs_data.Lock(INFINITE);          // find offline users
            ts->_this->topicality = i;
            std::map<CString, USER>::iterator eofTag = ts->_this->contactList.end();
            std::map<CString, USER>::iterator itrTag;
            for (itrTag = ts->_this->contactList.begin(); itrTag != eofTag; itrTag++)   // find offline users
            {
                if (ts->_this->topicality - 1 != itrTag->second._status)    // he is offline
                {
                    int id = ts->_this->m_ListComboBox.FindStringExact(-1, _T("+ ") + itrTag->first);
                    if (id != CB_ERR)
                    {
                        ts->_this->m_ListComboBox.DeleteString(id);
                        ts->_this->m_ListComboBox.AddString(_T("- ") + itrTag->first);
                        ::PostMessage(ts->_hwnd, MY_WM_UPDATE, (WPARAM)0, (LPARAM)0);
                    }
                }
            }
            ts->_this->cs_data.Unlock();
        }

        if (sendto(sock, msg, strlen(msg) + 1, 0, (struct sockaddr *) &addr, sizeof(addr)) != strlen(msg) + 1)
        {
            AfxMessageBox(_T("sendto!"));
        }
        Sleep(3000);
    }
    return 0;
}

UINT CChatDlg::recvThread(LPVOID param)
{
    THREADSTRUCT* ts = (THREADSTRUCT*)param;
    const int  ECHOMAX = 255;
    const unsigned short PORT = 1027;
    int sock;                        /* Socket */
    struct sockaddr_in servAddr;     /* Local address */
    struct sockaddr_in clntAddr;     /* Client address */
    struct sockaddr_in broadcastAddr;     /* Client address */

    char buffer[ECHOMAX];            /* Buffer for echo string */
    int cliLen;                      /* Length of incoming message */
    int recvMsgSize;                 /* Size of received message */


    /* Create socket for sending/receiving datagrams */
    if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
    {
        AfxMessageBox(_T("socket!"));
    }
    BOOL Val = TRUE;  // sock reuse

    if (SOCKET_ERROR == setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, (const char FAR *)&Val, sizeof(BOOL)))
    {
        AfxMessageBox(_T("Can`t set socket options."));
    }

    int broadcastPermission = 1;
    if (setsockopt(sock, SOL_SOCKET, SO_BROADCAST, (const char *)&broadcastPermission,
        sizeof(broadcastPermission)) < 0) {
        printf("setsockopt! %d", WSAGetLastError());
        getchar();
    }

    /* Construct local address structure */
    memset(&servAddr, 0, sizeof(servAddr));       /* Zero out structure */
    servAddr.sin_family = AF_INET;                /* Internet address family */
    servAddr.sin_addr.s_addr = htonl(INADDR_ANY); /* Any incoming interface */
    servAddr.sin_port = htons(PORT);              /* Local port */

    /* Bind to the local address */
    if (bind(sock, (struct sockaddr *) &servAddr, sizeof(servAddr)) != 0)
    {
        AfxMessageBox(_T("bind!"));
    }

    memset(&broadcastAddr, 0, sizeof(broadcastAddr));       /* Zero out structure */
    broadcastAddr.sin_family = AF_INET;                /* Internet address family */
    broadcastAddr.sin_addr.s_addr = htonl(INADDR_BROADCAST); /* Any incoming interface */
    broadcastAddr.sin_port = htons(PORT);              /* Local port */

    cliLen = sizeof(clntAddr);
    for (;;) /* Run forever */
    {

        if ((recvMsgSize = recvfrom(sock, buffer, ECHOMAX, 0, (struct sockaddr *) &clntAddr, &cliLen)) < 0)
        {
            AfxMessageBox(_T("resvMsgSize<0!"));
        }

        if (!strcmp("Hello", buffer))        // what did I receive?
        {                           // it's request to make friends
            ts->_this->cs_data.Lock(INFINITE);
            CString name = _T("N") + ts->_this->m_nameEdit;
            ts->_this->cs_data.Unlock();
            int len = sprintf_s(buffer, "%S", name);
            clntAddr.sin_port = htons(PORT);
            if (sendto(sock, buffer, len, 0, (struct sockaddr *) &broadcastAddr, sizeof(broadcastAddr)) != len)
            {
                AfxMessageBox(_T("sendto!"));
            }
        }
        else
        {
            if (!(strncmp("N", buffer, 1)))
            {
                char *name = new char[recvMsgSize];
                for (int i = 0; i < recvMsgSize - 1; i++)
                    name[i] = buffer[i + 1];
                name[recvMsgSize - 1] = NULL;
                CString cstring(name);
                delete[] name;
                ts->_this->cs_data.Lock(INFINITE);
                if (ts->_this->contactList.find(cstring) == ts->_this->contactList.end())  // new user
                {
                    USER usr;
                    usr._addr = clntAddr.sin_addr;
                    usr._status = ts->_this->topicality;
                    ts->_this->contactList.insert(std::pair<CString, USER>(cstring, usr));
                    ts->_this->m_ListComboBox.AddString((_T("+ ") + cstring));
                }
                else
                {                           // known user
                    int id = ts->_this->m_ListComboBox.FindStringExact(-1, _T("+ ") + cstring);
                    if (id != CB_ERR)
                    {                   // just update topicality cos usr is still online
                        USER usr;
                        usr._addr = clntAddr.sin_addr;
                        usr._status = ts->_this->topicality;
                        ts->_this->contactList[cstring] = usr;
                    }
                    else
                    {                   //user was offline
                        USER usr;
                        usr._addr = clntAddr.sin_addr;
                        usr._status = ts->_this->topicality;
                        ts->_this->contactList[cstring] = usr;
                        id = ts->_this->m_ListComboBox.FindStringExact(-1, _T("- ") + cstring);
                        ts->_this->m_ListComboBox.DeleteString(id);
                        ts->_this->m_ListComboBox.AddString(_T("+ ") + cstring);
                    }
                }
                int selectedIndex = ts->_this->m_ListComboBox.GetCurSel();
                if (CB_ERR != selectedIndex)
                {
                    CString selectedName;
                    ts->_this->m_ListComboBox.GetLBText(selectedIndex, selectedName);
                    ts->_this->m_addresseeComboBox = selectedName;
                }
                ts->_this->cs_data.Unlock();
                ::PostMessage(ts->_hwnd, MY_WM_UPDATE, (WPARAM)0, (LPARAM)0);
            }
            else
            {
                if (!(strncmp("M", buffer, 1)))
                {           // ������� ����, ����� �� ����
                    char nameLenC[3];
                    nameLenC[0] = buffer[1];
                    nameLenC[1] = buffer[2];
                    nameLenC[2] = NULL;
                    
                    char nameFromLenC[3];
                    nameFromLenC[0] = buffer[3];
                    nameFromLenC[1] = buffer[4];
                    nameFromLenC[2] = NULL;
                    //AfxMessageBox((CString)nameLenC);
                    int nameLen = atoi(nameLenC);
                    int nameFromLen = atoi(nameFromLenC);
                    char * nameC = new char[nameLen + 1];
                    for (int i = 0; i < nameLen; i++)
                    {
                        nameC[i] = buffer[i + 5];
                    }
                    nameC[nameLen] = NULL;
                    CString name(nameC);
                    delete[] nameC;
                    
                    char * nameFromC = new char[nameFromLen + 1];
                    for (int i = 0; i < nameFromLen; i++)
                    {
                        nameFromC[i] = buffer[i + 5 + nameLen];
                    }
                    nameFromC[nameFromLen] = NULL;
                    CString nameFrom(nameFromC);
                    delete[] nameFromC;
                    
                    if (name == ts->_this->m_nameEdit)
                    {
                        char *msg = new char[recvMsgSize - nameLen - 4 - nameFromLen];
                        for (int i = 0; i < recvMsgSize - nameLen - nameFromLen - 5; i++)
                            msg[i] = buffer[i + 5 + nameLen + nameFromLen];
                        msg[recvMsgSize - 5 - nameLen - nameFromLen] = NULL;
                        CString cstring(msg);
                        delete[] msg;
                        ts->_this->cs_data.Lock(INFINITE);

                        CString str = nameFrom + _T(": ") + cstring + ("\r\n");


                        ts->_this->m_logEdit = ts->_this->m_logEdit + str;
                        int nEnd = ts->_this->m_ctrlLog.GetWindowTextLength();
                        ts->_this->m_ctrlLog.SetSel(nEnd, nEnd);
                        ts->_this->m_ctrlLog.ReplaceSel(str);
                        std::ofstream f;
                        f.open(nameFrom + (CString)".txt", std::ios::app);
                        f << CT2A(str);
                        ts->_this->cs_data.Unlock();
                        ::PostMessage(ts->_hwnd, MY_WM_UPDATE, (WPARAM)0, (LPARAM)0);
                    }
                }
            }
        }
    }
}
void CChatDlg::OnBnClickedSendBtn()
{
    if (m_MsgEdit.GetLength() == 0) return;
    UpdateData(TRUE);
    const int  ECHOMAX = 255;
    const unsigned short PORT = 1027;
    int sock;                        /* Socket */
    struct sockaddr_in addr;         /* Client address */
    char buffer[ECHOMAX];            /* Buffer for echo string */
    if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
    {
        AfxMessageBox(_T("socket!"));
    }
    int broadcastPermission = 1;
    if (setsockopt(sock, SOL_SOCKET, SO_BROADCAST, (const char *)&broadcastPermission, sizeof(broadcastPermission)) < 0) {
        AfxMessageBox(_T("setsockopt!"));
    }
    int selectedIndex = m_ListComboBox.GetCurSel();
    CString name;
    m_ListComboBox.GetLBText(selectedIndex, name);
    if (name.Find(_T("- ")) == 0)
    {
        AfxMessageBox(_T("The user if offline"));
        return;
    }
    name.Delete(0, 2);
    /* Construct local address structure */
    memset(&addr, 0, sizeof(addr));   /* Zero out structure */
    addr.sin_family = AF_INET;        /* Internet address family */
    addr.sin_addr.s_addr = htonl(INADDR_BROADCAST);
    addr.sin_port = htons(PORT);      /* Local port */


    cs_data.Lock(INFINITE);                 // ������� ����, ����� �� ����  � name ��� ��� ����������

    CString nameLenS;
    nameLenS.Format((_T("%02d")), name.GetLength());
    CString nameFromLenS;
    nameFromLenS.Format((_T("%02d")), m_nameEdit.GetLength());
    CString msg = _T("M") + nameLenS + nameFromLenS + name + m_nameEdit + m_MsgEdit;

    int len = sprintf_s(buffer, "%S", msg);
    if (sendto(sock, buffer, len, 0, (struct sockaddr *) &addr, sizeof(addr)) != len)
    {
        AfxMessageBox(_T("sendto!"));
    }

    CString str = _T("me->") + name + _T(": ") + m_MsgEdit + ("\r\n");
    m_logEdit = m_logEdit + str;
    UpdateData(FALSE);
    cs_data.Unlock();
    std::ofstream f;
    f.open(name + (CString)".txt", std::ios::app);
    f << CT2A(str);
    f.close();
    m_MsgEdit = "";
    UpdateData(FALSE);
}


void CChatDlg::OnChangeNameEdit()
{
    UpdateData(TRUE);
    if (-1 != m_nameEdit.Find((CString)"/"))
        AfxMessageBox(_T("Please choose another name"));
    if (-1 != m_nameEdit.Find((CString)"\\"))
        AfxMessageBox(_T("Please choose another name"));
    if (-1 != m_nameEdit.Find((CString)":"))
        AfxMessageBox(_T("Please choose another name"));
    if (-1 != m_nameEdit.Find((CString)"?"))
        AfxMessageBox(_T("Please choose another name"));
    if (-1 != m_nameEdit.Find((CString)"<"))
        AfxMessageBox(_T("Please choose another name"));
    if (-1 != m_nameEdit.Find((CString)">"))
        AfxMessageBox(_T("Please choose another name"));
    if (-1 != m_nameEdit.Find((CString)"|"))
        AfxMessageBox(_T("Please choose another name"));
    if (-1 != m_nameEdit.Find((CString)"\""))
        AfxMessageBox(_T("Please choose another name"));
    cs_data.Lock(INFINITE);
    std::map<CString, USER>::iterator eofTag = contactList.end();
    std::map<CString, USER>::iterator itrTag;
    for (itrTag = contactList.begin(); itrTag != eofTag; itrTag++)
    {
        if (itrTag->first == m_nameEdit)
            AfxMessageBox(_T("Please choose another name"));
    }
    cs_data.Unlock();
}


void CChatDlg::OnChangeMsgEdit()
{
    UpdateData(TRUE);
}


void CChatDlg::OnBnClickedHistoryBtn()
{
    int selectedIndex = m_ListComboBox.GetCurSel();
    CString name;
    m_ListComboBox.GetLBText(selectedIndex, name);
    CHistoryDlg *History = new CHistoryDlg(name, this);

    BOOL ret = History->Create(IDD_HISTORY_DIALOG, this);

    if (!ret)   //Create failed.
    {
        AfxMessageBox(_T("Error creating Log Dialog"));
    }
    History->ShowWindow(SW_SHOW);
}
