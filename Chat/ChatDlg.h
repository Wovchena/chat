
// ChatDlg.h : header file
//
#include <map>
#pragma once
#define MY_WM_UPDATE  (WM_APP + 1)

// CChatDlg dialog
class CChatDlg : public CDialogEx
{
// Construction
public:
	CChatDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_CHAT_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
    typedef struct THREADSTRUCT
    {
        CChatDlg*    _this;
        HWND _hwnd;
        //you can add here other parameters you might be interested on
    } THREADSTRUCT;
    static UINT recvThread(LPVOID param);
    static UINT sendThread(LPVOID param);
// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
    afx_msg LRESULT OnMyUpdate(UINT wParam, LONG lParam);
    CComboBox m_ListComboBox;
    afx_msg void OnBnClickedSendBtn();
    CString m_MsgEdit;
    CEdit m_ctrlLog;
    CString m_addresseeComboBox;
    typedef struct USER
    {
        IN_ADDR _addr;
        unsigned long long _status;
    };
    unsigned long long topicality;
    std::map <CString, USER> contactList;
    CString m_nameEdit;
    afx_msg void OnChangeNameEdit();
    afx_msg void OnChangeMsgEdit();
    CString m_logEdit;
    CCriticalSection cs_data;           // for critical sections
    afx_msg void OnBnClickedHistoryBtn();
};

