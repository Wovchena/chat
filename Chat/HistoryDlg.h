#pragma once


// CHistoryDlg dialog

class CHistoryDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CHistoryDlg)

public:
	CHistoryDlg(CWnd* pParent = NULL);   // standard constructor
    CHistoryDlg(CString name, CWnd * pParent = NULL);
	virtual ~CHistoryDlg();

// Dialog Data
	enum { IDD = IDD_HISTORY_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
    CString m_historyEdit;
};
