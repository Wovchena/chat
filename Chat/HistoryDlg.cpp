// HistoryDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Chat.h"
#include "HistoryDlg.h"
#include "afxdialogex.h"
#include <iostream>     // for files
#include <fstream>      // for files
#include <string.h>


// CHistoryDlg dialog

IMPLEMENT_DYNAMIC(CHistoryDlg, CDialogEx)

CHistoryDlg::CHistoryDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CHistoryDlg::IDD, pParent)
    , m_historyEdit(_T(""))
{

}

CHistoryDlg::CHistoryDlg(CString name, CWnd* pParent) : CDialogEx(CHistoryDlg::IDD, pParent)
{
    name.Delete(0, 2);
    std::ifstream f(name+(CString)".txt");
    std::string tmp;
    while (!f.eof())
    {
        char buff[255];
        f.getline(buff, 255);
        CString stmp = CString(buff) + ("\r\n");
        m_historyEdit += stmp;
    }
    f.close();
}

CHistoryDlg::~CHistoryDlg()
{
}

void CHistoryDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
    DDX_Text(pDX, IDC_HISTORY_EDIT, m_historyEdit);
}


BEGIN_MESSAGE_MAP(CHistoryDlg, CDialogEx)
END_MESSAGE_MAP()


// CHistoryDlg message handlers
